"use strict";
Object.defineProperties(exports, { __esModule: { value: true }, [Symbol.toStringTag]: { value: "Module" } });
var vue = require("vue");
var serverRenderer = require("vue/server-renderer");
var pluginVue_exportHelper = require("./assets/plugin-vue_export-helper.db096aab.js");
var index_md_vue_type_style_index_0_lang = "";
const __pageData = JSON.parse('{"title":"k-button \u6309\u94AE","description":"","frontmatter":{},"headers":[{"level":2,"title":"\u57FA\u7840\u7528\u6CD5","slug":"\u57FA\u7840\u7528\u6CD5"},{"level":2,"title":"\u7981\u7528\u72B6\u6001","slug":"\u7981\u7528\u72B6\u6001"},{"level":2,"title":"\u8C03\u6574\u5C3A\u5BF8","slug":"\u8C03\u6574\u5C3A\u5BF8"}],"relativePath":"examples/button/index.md"}');
const _sfc_main = { name: "examples/button/index.md" };
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_k_button = vue.resolveComponent("k-button");
  _push(`<div${serverRenderer.ssrRenderAttrs(_attrs)}><h1 id="k-button-\u6309\u94AE" tabindex="-1">k-button \u6309\u94AE <a class="header-anchor" href="#k-button-\u6309\u94AE" aria-hidden="true">#</a></h1><h2 id="\u57FA\u7840\u7528\u6CD5" tabindex="-1">\u57FA\u7840\u7528\u6CD5 <a class="header-anchor" href="#\u57FA\u7840\u7528\u6CD5" aria-hidden="true">#</a></h2><p>\u4F7F\u7528 type\u3001plain\u3001round \u548C circle \u6765\u5B9A\u4E49\u6309\u94AE\u7684\u6837\u5F0F\u3002</p><div class="example"><div>`);
  _push(serverRenderer.ssrRenderComponent(_component_k_button, null, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u9ED8\u8BA4\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u9ED8\u8BA4\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { type: "primary" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4E3B\u8981\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4E3B\u8981\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { type: "success" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u6210\u529F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u6210\u529F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { type: "info" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4FE1\u606F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4FE1\u606F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { type: "warning" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u8B66\u544A\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u8B66\u544A\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { type: "danger" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u5371\u9669\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u5371\u9669\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { type: "text" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u6587\u5B57\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u6587\u5B57\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`<br><br>`);
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { plain: "" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u6734\u7D20\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u6734\u7D20\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "primary",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4E3B\u8981\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4E3B\u8981\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "success",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u6210\u529F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u6210\u529F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "info",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4FE1\u606F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4FE1\u606F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "warning",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u8B66\u544A\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u8B66\u544A\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "danger",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u5371\u9669\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u5371\u9669\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`<br><br>`);
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { round: "" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u5706\u89D2\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u5706\u89D2\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "primary",
    round: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4E3B\u8981\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4E3B\u8981\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "success",
    round: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u6210\u529F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u6210\u529F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "info",
    round: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4FE1\u606F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4FE1\u606F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "warning",
    round: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u8B66\u544A\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u8B66\u544A\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "danger",
    round: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u5371\u9669\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u5371\u9669\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div><details><summary>\u5C55\u5F00\u67E5\u770B</summary><div class="language-vue"><span class="copy"></span><pre><code><span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">template</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">div</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u9ED8\u8BA4\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">primary</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4E3B\u8981\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">success</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u6210\u529F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">info</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4FE1\u606F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">warning</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u8B66\u544A\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">danger</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u5371\u9669\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">text</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u6587\u5B57\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">br</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> /&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">br</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> /&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u6734\u7D20\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">primary</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4E3B\u8981\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">success</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u6210\u529F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">info</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4FE1\u606F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">warning</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u8B66\u544A\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">danger</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u5371\u9669\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">br</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> /&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">br</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> /&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">round</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u5706\u89D2\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">primary</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">round</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4E3B\u8981\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">success</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">round</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u6210\u529F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">info</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">round</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4FE1\u606F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">warning</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">round</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u8B66\u544A\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">danger</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">round</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u5371\u9669\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">div</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">template</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">script</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">lang</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">ts</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">setup</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF", "font-style": "italic" })}">import</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">{</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">k</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">-</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">}</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF", "font-style": "italic" })}">from</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">kitty-ui</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">script</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">style</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">.</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#FFCB6B" })}">k-k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">{</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#B2CCD6" })}">margin-right</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">:</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F78C6C" })}">10px</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">}</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">style</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"></span></code></pre></div></details><h2 id="\u7981\u7528\u72B6\u6001" tabindex="-1">\u7981\u7528\u72B6\u6001 <a class="header-anchor" href="#\u7981\u7528\u72B6\u6001" aria-hidden="true">#</a></h2><div class="example"><div>`);
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { disabled: "" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u7981\u7528\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u7981\u7528\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "primary",
    disabled: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4E3B\u8981\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4E3B\u8981\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "success",
    disabled: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u6210\u529F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u6210\u529F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "info",
    disabled: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4FE1\u606F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4FE1\u606F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "warning",
    disabled: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u8B66\u544A\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u8B66\u544A\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "danger",
    disabled: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u5371\u9669\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u5371\u9669\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`<br><br>`);
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { disabled: "" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u7981\u7528\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u7981\u7528\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "primary",
    disabled: "",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4E3B\u8981\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4E3B\u8981\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "success",
    disabled: "",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u6210\u529F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u6210\u529F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "info",
    disabled: "",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4FE1\u606F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4FE1\u606F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "warning",
    disabled: "",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u8B66\u544A\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u8B66\u544A\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, {
    type: "danger",
    disabled: "",
    plain: ""
  }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u5371\u9669\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u5371\u9669\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div><details><summary>\u5C55\u5F00\u67E5\u770B</summary><div class="language-vue"><span class="copy"></span><pre><code><span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">template</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">div</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u7981\u7528\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">primary</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4E3B\u8981\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">success</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u6210\u529F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">info</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4FE1\u606F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">warning</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u8B66\u544A\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">danger</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u5371\u9669\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">br</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> /&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">br</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> /&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u7981\u7528\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">primary</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4E3B\u8981\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">success</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u6210\u529F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">info</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4FE1\u606F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">warning</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u8B66\u544A\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">type</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">danger</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">disabled</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">plain</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u5371\u9669\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">div</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">template</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">script</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">lang</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">ts</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">setup</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF", "font-style": "italic" })}">import</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">{</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">k</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">-</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">}</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF", "font-style": "italic" })}">from</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">kitty-ui</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">script</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">style</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">.</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#FFCB6B" })}">k-k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">{</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#B2CCD6" })}">margin-right</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">:</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F78C6C" })}">10px</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">}</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">style</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"></span></code></pre></div></details><h2 id="\u8C03\u6574\u5C3A\u5BF8" tabindex="-1">\u8C03\u6574\u5C3A\u5BF8 <a class="header-anchor" href="#\u8C03\u6574\u5C3A\u5BF8" aria-hidden="true">#</a></h2><div class="example"><div>`);
  _push(serverRenderer.ssrRenderComponent(_component_k_button, null, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u9ED8\u8BA4\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u9ED8\u8BA4\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { size: "medium" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u4E2D\u7B49\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u4E2D\u7B49\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { size: "small" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u5C0F\u578B\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u5C0F\u578B\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(serverRenderer.ssrRenderComponent(_component_k_button, { size: "mini" }, {
    default: vue.withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`\u8D85\u5C0F\u6309\u94AE`);
      } else {
        return [
          vue.createTextVNode("\u8D85\u5C0F\u6309\u94AE")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div><details><summary>\u5C55\u5F00\u67E5\u770B</summary><div class="language-vue"><span class="copy"></span><pre><code><span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">template</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">div</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u9ED8\u8BA4\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">size</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">medium</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u4E2D\u7B49\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">size</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">small</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u5C0F\u578B\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">    </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">size</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">mini</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">\u8D85\u5C0F\u6309\u94AE</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">div</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">template</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">script</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">lang</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">=</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">ts</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C792EA" })}">setup</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF", "font-style": "italic" })}">import</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">{</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">k</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">-</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">}</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF", "font-style": "italic" })}">from</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#C3E88D" })}">kitty-ui</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&quot;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">script</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">style</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">.</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#FFCB6B" })}">k-k-button</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">{</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}">  </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#B2CCD6" })}">margin-right</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">:</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#A6ACCD" })}"> </span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F78C6C" })}">10px</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">;</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">}</span></span>
<span class="line"><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&lt;/</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#F07178" })}">style</span><span style="${serverRenderer.ssrRenderStyle({ "color": "#89DDFF" })}">&gt;</span></span>
<span class="line"></span></code></pre></div></details></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = vue.useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("examples/button/index.md");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
var index = /* @__PURE__ */ pluginVue_exportHelper._export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
exports.__pageData = __pageData;
exports["default"] = index;
